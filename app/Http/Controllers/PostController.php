<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use DB;

class PostController extends Controller
{
    public function show($post)
    {
      $iValueReturned = 3;

      if ($iValueReturned==1) return 'Hello';
      elseif ($iValueReturned==2)
      {
        $aPosts = [
            1=>'This is my first about post blog',
            2=>'This is my 2de about post blog',
            3=>'This is my 3de about post blog'
          ];
          //Display a 404 message if key does not apc_exists
          if (($post>0) && !array_key_exists($post, $aPosts))
          {
            abort(404, 'Sorry, that post was not found.');
          }

          $sPostVal = (!$post || $post==0) ? '' : $aPosts[$post];
      }
      elseif($iValueReturned==3)
      {
        //$aPost = DB::table('posts')->where('id', $post)->first();
        // $aPosts = Post::where('emp_id', $post)->firstOrFail();
        $aPosts = Post::find($post);
        //dd($aPosts);
        //Display a 404 message if key does not apc_exists
        if (($post>0) && ! $aPosts)
        {
          abort(404, 'Sorry, that post was not found.');
        }

        $sPostVal = $aPosts->description;
      }
      else {
        return view('post', [
           'aPosts'=> Post::where('emp_id', $post)->firstOrFail(),
           'sPostVal'=> 'Retrieve form the DB post'
          ] );
      }

      //dd($sPostVal);
      return view('post', compact( 'aPosts', 'sPostVal', 'post', 'iValueReturned'));
    }
}

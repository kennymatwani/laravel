<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articles;

class ArticlesController extends Controller
{
    public function index()
    {
      //render a list of resources
      $aArticles = Articles::take(3)->latest()->get();
      return view('articles.index', compact('aArticles'));
    }

    /*public function show($id)
    {
      //render 1/single  resources
      //$article = Articles::find($id);
      //or
      $aArticle = Articles::findOrFail($id);
      //dd($article);
      return view('articles.show', ['article'=> $article]);
    }*/
    //or
    public function show(Articles $article)
    {
      //render 1/single  resources
      //dd($article);
      return view('articles.show', ['article'=> $article]);
    }

    //Mnipulation actions
    public function create()
    {
      //Show a view to create a new resource
      return view('articles.create');
    }

    public function store()
    {
      //dump(request()->all());
      $aValidatedRequests = request()->validate([
        'title'=>['required', 'min:3', 'max:255'],
        'excerpt'=>'required',
        'body'=>'required'
      ]);

      //Persist the created resource from the create function(kind of commit)
      /*
      $article = new Articles();
      $article->title = request('title');
      $article->excerpt = request('excerpt');
      $article->body = request('body');
      $article->save();
      */
      //or you can se no need of the ave functionality has it will save it directly
      /*Articles::create([
        'title'=> request('title'),
        'excerpt'=> request('excerpt'),
        'body'=> request('body')
      ]);*/
      //or
      Articles::create($aValidatedRequests);
      //or to really narrow down your code and make it cleaner narrow down the mass saving in validation
      /*
      Articles::create(request()->validate([
        'title'=>['required', 'min:3', 'max:255'],
        'excerpt'=>'required',
        'body'=>'required'
      ]));
      */
      //or
      //Articles::create($this->validateArticle());

      return redirect('/articles');
    }

    public function edit($id)
    {
      //Show a view to edit existing resource
      $aArticle = Articles::find($id);
      return view('articles.edit', compact('aArticle'));
    }

    public function update($id)
    {
      $aValidatedRequests = request()->validate([
        'title'=>['required', 'min:3', 'max:255'],
        'excerpt'=>'required',
        'body'=>'required'
      ]);

      //Persist the edited resource from the edit function(kind of commit)
      $aArticle = Articles::find($id);
      $aArticle->title = request('title');
      $aArticle->excerpt = request('excerpt');
      $aArticle->body = request('body');
      $aArticle->save();
      //or
      //Articles::update($aValidatedRequests);
      //or if you have passed the Article $article in the fann_get_cascade_activation_functions
      //$article->update($aValidatedRequests);
      //or the shorter sqlite_libversion
      /*
      $article->update(request()->validate([
        'title'=>['required', 'min:3', 'max:255'],
        'excerpt'=>'required',
        'body'=>'required'
      ]));
      */
      //or
      //$article->update($this->validateArticle());

      //dump(request()->all());
      //dd($sTitle);
      //dd('Hello');
      return redirect('/articles/'.$aArticle->id);
      //or
      return redirect(route('articles.show', $aArticle->id));
      //or if you have declare a path function in the Model and you using the sent resource in the function e.g: function (Articles $article)
      //return redirect($article->path())
    }

    public function destroy($id)
    {
      //delete a resource
      //dd($id);
      $article = Articles::findOrFail($id);
      $article->delete();

      return redirect(route('articles.index'))->with([
          'flash_message' => 'Deleted',
          'flash_message_important' => false
        ]);
    }

    public function validateArticle()
    {
      return request()->validate([
        'title'=>['required', 'min:3', 'max:255'],
        'excerpt'=>'required',
        'body'=>'required'
      ]);
    }
}

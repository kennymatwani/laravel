<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Tasks extends Model
{
    use HasFactory;

    //The below function will help you update the query on the databse
    public function isComplete()
    {
      $iReturnValue = 1;
      if (!$iReturnValue) return false;
      else {
        $this->completion = true;
        $this->save();
        return 'Successfuly Added';
      }
    }

    public function changeComplete()
    {
      $iReturnValue = 1;
      if (!$iReturnValue) return false;
      else {
        $this->completion = false;
        $this->save();
        return 'Modified';
      }
    }

    public static function yourTask($id)
    {
      return static::where('emp_id', $id)->get();
    }

    //relationship
    public function user()
    {
      return $this->belongsTo(User::class);
    }

}

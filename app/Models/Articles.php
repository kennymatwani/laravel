<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Articles extends Model
{
    use HasFactory;

    //protected $guarded = []; //to remove the mass assignement protection -->
    protected $fillable = ['title', 'excerpt', 'body']; //Declaring variable that can be mass assigned
    //Add this function in order to change the cloumn search for the wild card
    /*public function getRouteKeyName()
    {
      return 'title';
    }*/

    public function path()
    {
      return route('articles.show', $this);
    }

    //relationship
    //if function name is user Laravel will assume that the foreign key is user_id, but if you change the name you trader_will
    //you will need to pass on a parameter to overidde(in this case it will assume author_id)
    public function author()
    {
      return $this->belongsTo(User::class, 'user_id');
    }

    //To review as creation on tinker comlain regarding the tables
    public function tags()
    {
      return $this->belongsToMany(Tag::class);
    }
}

?>

@extends('layouts.master')


@section('content')

<h2>Firstname:  {{ $aEmployee->firstname }} </h2>
<h2>Surname:  {{ $aEmployee->surname }} </h2>
<h2>Mail: <a mailto="{{ $aEmployee->mail }}"> {{ $aEmployee->mail }} </a></h2>

Your tasks: </br>
@if($aTasks->isEmpty())
  <p>There is/are no task(s) loaded for you!</p>
@else
<ul>
@foreach($aTasks as $otask)

<li> {{ $otask->id }}. {{ $otask->title }} </li>

@endforeach
</ul>
@endif

@endsection

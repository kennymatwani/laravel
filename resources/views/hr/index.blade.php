@extends('layouts.master')

@section('content')

<style>
.color-blue { color: blue;}
.color-red { color: red;}
.is-loading { background-color: yellow;}
</style>
<!--Area of existence-->

<div id="vue_root_area1">
  <h1 :class="className">Index page reached</h1>
  Today's Date: <h2 v-text="new Date()"></h2>
  <h3>@{{ reversedMessage }}</h3>
  <br><br>
  <label>1st text: </label><input type="text" id="input" v-model="message"/>
  <label>2de text: </label><input type="text" id="input2" v-model="message2"/>
  </br></br>
  <p>When Changing the 1st text input; the text changes will appear here  @{{ message }}</p></br>
  <p>The 2de Test input is:@{{ message2 }}</p>

  <label>Array Listing:</label></br>
  <ul>
    <li v-for="name in names">@{{ name }}</li>
  </ul>

  <input id="add_name" type="text" v-model="newName" :placeholder="addNewTitle">
  <button id="add_btn" @click="addName">Add Name</button>
  <button :class="{'is-loading': isLoading }" :disabled="isDisabled" v-bind:title="btnTitle" @click="removeName" id="remove_btn">Remove Name</button>

  </br></br>
  <h1 :class="className">TASKS</h1>
  <label>TODO:</label></br>
  <ul>
    <li v-for="task in tasks" v-if="!task.completed" v-text="task.description"></li>
  </ul>

  <label>Completed tasks Only</label></br>
  <ul>
    <li v-for="task in tasks" v-if="task.completed" v-text="task.description"></li>
  </ul>

  <label>Incomplete tasks Only</label></br>
  <ul>
    <li v-for="task in incompletTtasks" v-text="task.description"></li>
  </ul>

  <p>This is Just to demonstrate how powerful VueJS is!</p>

</div>
<script>
      let/*or var*/ data = { message: 'Hello World'};
      //document.getElementById('input').value=data.message;

      var vueApp = new Vue({
        el:'#vue_root_area1',
        //data: data
        data:
        {
          addNewTitle: 'Add a new name in here then save',
          isDisabled: false,
          isLoading: false,
          className: 'color-blue',
          btnTitle: 'Biding the title to the button trought Vue JS',
          newName: '',
          message: "Hellow world",
          message2: "Vue Js ",
          //names:['Nanou','Patrick', 'Noemie', 'Kenny', 'Joel','Rosine', 'Jean-Michel', 'Unknown']
          names: ['Kenny','Ira', 'Keira-Belle', 'Hazel-Grace', 'Tristan'],
          tasks: [
            {description: 'Go to the Store', completed: true},
            {description: 'Finish Laravel Trainig', completed: false},
            {description: 'Finish VueJS Trainig', completed: false},
            {description: 'Finish Typing Script Trainig', completed: false},
            {description: 'Learn Git', completed: true},
            {description: 'Go to church', completed: true},
            {description: 'Save Laravel Projec tin Repository', completed: true}
          ]
        },

        //Mounted method
        //mounted(){
          //alert('aret ready');
        //}

        methods: {
          addName() {
            //alert('add name');
            this.names.push(this.newName);
            this.newName='';
          },

          removeName() {
            var iLength= this.names.length;
            var aRandom = [];
            for(i=0; i<iLength; i++) aRandom.push(i);

            var iRandom = aRandom[Math.floor(Math.random() * iLength)];

            //console.log(aRandom+' Random is:'+iRandom);
            if (iLength>0) {
              //alert('Remove Name: '+iLength);
              this.names.splice(iRandom, 1); //remove value form array
              this.isLoading = true;
              this.isDisabled = false;
            }
            else {
              alert('Nothing to remove');
              this.isLoading = false;
              this.isDisabled = true;
            }
          }
        },

        computed: {
          reversedMessage() {
            return this.message.split('').reverse('').join('');
          }
        }
      });
</script>


</br>
</br>
<h1>
  Names of employees in the Database:
</h1>
</br>

<table border="2">
  <tr>
      <th>&nbsp;</th>
      <th nowrap>Emp ID</th>
      <th>Firstname</th>
      <th>Surname</th>
      <th>Email</th>
  </tr>
  @foreach($aEmployees as $sEmp)
    <tr>
      <td><a href='/hr/{{ $sEmp->id }}'>View</a></th>
      <td style="text-align:center">{{ $sEmp->id }}</td>
      <td>{{ $sEmp->firstname }}</td>
      <td>{{ $sEmp->surname }}</td>
      <td>{{ $sEmp->email }}</td>
    </tr>

  @endforeach

</table>


@endsection

@section('footer')

<h1>Footer</h1>
@include('stats')
@endsection

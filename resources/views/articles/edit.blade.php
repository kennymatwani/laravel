@extends('layouts.master')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
@endsection

@section('content')

  <h1 class="heading has-text-weight-bold is-size-5">Update Article</h1>
    <div class="container">
        <form method="POST" action="/articles/{{ $aArticle->id  }}">
          @csrf
          @method('PUT')

          <div class="field">
            <label class="label" for="title">Title</label>
              <div class="control">
                <input class="input" type="text" name="title" id="title" value="{{ $aArticle->title }}">
                <p class="help is-danger">{{ $errors->first('title') }}</p>
              </div>
          </div>

          <div class="field">
            <label class="label" for="excerpt">Excerpt</label>
              <div class="control">
                <textarea class="textarea" type="text" name="excerpt" id="excerpt" >{{ $aArticle->excerpt }}</textarea>
                  <p class="help is-danger">{{ $errors->first('excerpt') }}</p>
              </div>
          </div>

          <div class="field">
            <label class="label" for="body">Body</label>
              <div class="control">
                <textarea class="textarea" type="text" name="body" id="body" >{{ $aArticle->body }}</textarea>
              </div>
          </div>

          <div class="field">
              <div class="control">
                <button class="button is-link" type="submit" id="submit_btn">Update</button>
              </div>
          </div>
        </form>
    </div>
@endsection

@section('footer')
@endsection

@extends('layouts.master')

@section('content')
<h2><b><u>Articles</u></b></h2>
@if($aArticles->isEmpty())
<em>No article to display</em>
@else
  @foreach($aArticles as $article)
    <article>
    <!--  <h3><a style="cursor:pointer" href="/articles/{{ $article->id }}">{{ $article->title }}</a></h3>-->
      <h3><a style="cursor:pointer" href="{{ route('articles.show' ,$article->id) }}">{{ $article->title }}</a></h3>
      <p>{{ $article->body }}</p>
    </article>
  @endforeach
@endif

Click <a style="cursor:pointer" href="/articles/create">HERE</a> to create a new article

@endsection

@section('footer')

<h1>Footer</h1>

@endsection

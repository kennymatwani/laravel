@extends('layouts.master')

@section('content')

<h3>{{ $article->title }}</h3>
<p>{{ $article->body }}</p>

Click <a style="cursor:pointer" href="/articles/{{ $article->id }}/edit">HERE</a> to edit this article or
click on <a style="cursor:pointer" href="{{ route('articles.delete', $article->id) }}">DELETE</a> to erase it.


@endsection

@section('footer')
@endsection

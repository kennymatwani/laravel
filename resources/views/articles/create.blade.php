@extends('layouts.master')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
@endsection

@section('content')

  <h1 class="heading has-text-weight-bold is-size-5">New Article</h1>
    <div class="container">
        <form method="POST" action="/articles">
          @csrf
          <div class="field">
            <label class="label" for="title">Title</label>
              <div class="control">
                <input class="input @error('title') is-danger @enderror" type="text" name="title" id="title" value="{{ old('title') }}">

                @error('title')
                  <p class="help is-danger">{{ $errors->first('title') }}</p>
                @enderror
              </div>
          </div>

          <div class="field">
            <label class="label" for="excerpt">Excerpt</label>
              <div class="control">
                <textarea class="textarea {{ $errors->has('excerpt') ? 'is-danger' : '' }}" type="text" name="excerpt" id="excerpt">{{ old('excerpt') }}</textarea>
                @if($errors->has('excerpt'))
                  <p class="help is-danger">{{ $errors->first('excerpt') }}</p>
                @endif
              </div>
          </div>

          <div class="field">
            <label class="label" for="body">Body</label>
              <div class="control">
                <textarea class="textarea {{ $errors->has('body') ? 'is-danger' : '' }}" type="text" name="body" id="body">{{ old('body') }}</textarea>
                @if($errors->has('body'))
                  <p class="help is-danger">{{ $errors->first('body') }}</p>
                @endif
              </div>
          </div>

          <div class="field">
              <div class="control">
                <button class="button is-link" type="submit" id="submit_btn">Submit</button>
              </div>
          </div>
        </form>
    </div>
@endsection

@section('footer')
@endsection

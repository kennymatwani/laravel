@extends('layouts.master')

@section('content')

<h1>About laravel and Docker</h1>

Parameter input sent torught URL: {{ $aRequestName }}
<!--If parameter is javascript-->
{!! $aRequestName !!}

Retrieving the posted wildcard infos.

<h2><b><u>Articles</u></b></h2>
@if($aArticles->isEmpty())
<em>No article to display</em>
@else
  @foreach($aArticles as $article)
    <article>
      <h3>{{ $article->title }}</h3>
      <p>{{ $article->body }}</p>
    </article>
  @endforeach
@endif

@endsection


@section('footer')

<h1>Footer</h1>

@endsection

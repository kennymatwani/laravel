<!--Injecting and binding service
Class created under App\Providers to bind function(services) directly to your blade
When composing the view i want that view to run if stat was bind in your page
Resolve service directly to blade view
-->
@inject('stats', 'App\Providers\Stats')

<h3>STATS</h3>

<!--Class created under App\Providers to bind function(services) directly to your blade-->
{{ $stats->lessons() }}

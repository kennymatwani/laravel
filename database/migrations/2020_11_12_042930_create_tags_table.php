<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tags')){
          Schema::create('tags', function (Blueprint $table) {
              $table->increments('id');
              $table->string('name');
              $table->timestamps();
          });
        }

        //relastionship between 2 tables article_tag ->brige table this represent a many to many relatiosnhip
        Schema::create('articles_tag', function (Blueprint $table) {
            $table->increments('id');
            //$table->unsignedBigIncrements('article_id');
            $table->unsignedInteger('articles_id')->index();
            //$table->unsignedBigIncrements('tag_id');
            $table->unsignedInteger('tag_id')->index();
            $table->string('name');
            $table->timestamps();

            //Unique key
            $table->unique('articles_id', 'tag_id');
            $table->foreign('articles_id')->references('id')->on('articles')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Employees;
use App\Models\Tasks;
use App\Models\Articles;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(2)->create();

        User::factory(2)->create();
        Employees::factory(2)->create();
        Tasks::factory(2)->create();
        Articles::factory(5)->create();

        //For Larave 5.1
        //User::truncate();
        //factory(User::class, 2)->create();
    }
}

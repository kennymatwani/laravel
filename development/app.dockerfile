FROM php:7.3-fpm

COPY composer.lock composer.json /var/www/


COPY database /var/www/database


WORKDIR /var/www


RUN apt-get update
RUN apt-get -y install git
RUN apt-get -y install zip


RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN php composer.phar install --no-dev --no-scripts
RUN rm composer.phar


COPY . /var/www


RUN chown -R www-data:www-data /var/www/storage
RUN chown -R www-data:www-data /var/www/bootstrap/cache


RUN php artisan cache:clear


RUN php artisan optimize


RUN apt-get install -y libmcrypt-dev libmagickwand-dev --no-install-recommends

RUN pecl install mcrypt-1.0.2\
docker-php-ext-enable mcrypt

RUN docker-php-ext-install pdo_mysql

RUN mv .env.prod .env


RUN php artisan optimize

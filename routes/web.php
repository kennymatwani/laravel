<?php

use Illuminate\Support\Facades\Route;
use App\Models\Employees;
use App\Models\Articles;

use App\Http\Controllers\PostController;
use App\Http\Controllers\ArticlesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Injecting and binding service
//Class created under App\Providers to bind function(services) directly to your blade
//When composing the view i want that view to run if stat was bind in your page
//Resolve service directly to blade view
/*View::composer('stats', function($view){
    $view->with('stats', app('App\Providers\Stats'));
});*/

Route::get('/', function () {
//dd("NOt reaching route anymore");
    $sPersonName = 'Kenny';
    return view('welcome', compact('sPersonName'));
});

Route::get('/about', function () {

  //When parameter is sent trough URI
    $aRequestName = request('name');

    $aArticles = Articles::take(3)->latest()->get();
    //dd($aArticles);
    return view('about', compact('aRequestName', 'aArticles'));
})->name('about.view');

//REST
//TO Apply the Restful action we do no past the action on the link but make uses of the HTTP verbs: GET, POST, PUT or PATCH, DELETE
/*
  GET /articles
  GET /article/{id}
  POST /articles
  PUT /articles/{id}
  DELETE /articles/{id}
*/

Route::get('/articles', [ArticlesController::class, 'index'])->name('articles.index'); //list all resources -->point to a page

//when resource is posted  send it to the store function to be saved
Route::post('/articles', [ArticlesController::class, 'store']); // redirect to a page
//Ordering matter for the rest usage, creation should come before the showing of single resource
Route::get('articles/create', [ArticlesController::class, 'create'])->name('articles.create'); //point to a page

//View 1
Route::get('/articles/{article}', [ArticlesController::class, 'show'])->name('articles.show'); // show a single resource -->point to a page
Route::get('/articles/{article}/destroy', [ArticlesController::class, 'destroy'])->name('articles.delete'); //get single resource to display for edit
Route::get('/articles/{article}/edit', [ArticlesController::class, 'edit'])->name('articles.edit'); //get single resource to display for edit
Route::put('/articles/{article}', [ArticlesController::class, 'update']); //update edited resource --redirect to a page or better the edited resource

/*
Route::get('/post/{post}', function ($post) {

  //When parameter is sent trough URI
    $aPosts = [
      1=>'This is my first about post blog',
      2=>'This is my 2de about post blog',
      3=>'This is my 3de about post blog'
    ];

    //Display a 404 message if key does not apc_exists
    if (($post>0) && !array_key_exists($post, $aPosts))
    {
      abort(404, 'Sorry, that post was not found.');
    }

    $sPostVal = (!$post || $post==0) ? '' : $aPosts[$post];

    return view('post', compact( 'aPosts', 'sPostVal', 'post'));
});*/

//Or make use of your controller to do all those above actions

//Route with Controller_Abstract
//Route::get('/post/{post}', 'PostController@show');
//Route::get('/post/{post}', 'App\Http\Controllers\RegisterControlle@show');
Route::get('/post/{post}',  [PostController::class, 'show']);

//To review for test factory loading
Route::get('/hr/details', function () {
  $aEmp = App\Models\Employees::all();
    //return view('/hr/details')->with('/hr/details', App\Models\Employees::all());
    return view('/hr/details')->with('/hr/details', $aEmp);
});

Route::get('hr', function () {
  $sName = 'Kenny';
  $aTasks = [
      'Finishing Laravel Video',
      'WHat is VueJS',
      'God is Love'
    ];

    $aEmployees = App\Models\employees::all();
    return view('hr.index', compact('aTasks', 'sName', 'aEmployees'));
});


Route::get('/hr/{id}', function ($id){
  /*dd($id);*/
  //$aEmployee = App\Models\employees::get($id);
  $aEmployee = App\Models\employees::find($id);
  //$aEmployee = DB::table('employees')->find($id);
  $aTasks = tasks::yourTask($id);

  return view('hr.show', compact('aEmployee', 'aTasks'));
});
